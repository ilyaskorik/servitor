package servitor

import grails.transaction.Transactional
import org.codehaus.groovy.grails.io.support.GrailsResourceUtils
import org.codehaus.groovy.grails.plugins.GrailsPluginManager
import org.codehaus.groovy.grails.web.pages.discovery.GrailsConventionGroovyPageLocator
import org.codehaus.groovy.grails.web.servlet.GrailsApplicationAttributes
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsWebRequest
import org.springframework.web.context.request.RequestAttributes
import org.springframework.web.context.request.RequestContextHolder

@Transactional
class TemplateUtilsService {

    GrailsPluginManager pluginManager
    GrailsConventionGroovyPageLocator groovyPageLocator

    def findTemplate() {
        def candidatePaths = candidateTemplatePaths(controllerNamespace, controllerName, actionName)
        candidatePaths.findResult { path ->
            def source = groovyPageLocator.findViewByPath(path)
            if (source) {
                source.URI
            } else {
                null
            }
        }
    }

    private List<String> candidateTemplatePaths(String controllerNamespace, String controllerName, String actionName) {

        def templateResolveOrder = []

        if (controllerNamespace) {
            templateResolveOrder << GrailsResourceUtils.appendPiecesForUri("/", controllerNamespace, controllerName, actionName)
        }

        if (controllerName) {
            templateResolveOrder << GrailsResourceUtils.appendPiecesForUri("/", controllerName, actionName)
        }

        templateResolveOrder << GrailsResourceUtils.appendPiecesForUri("/", "servitorDefault", actionName)

        templateResolveOrder
    }

    private String getControllerNamespace() {
        if (GrailsWebRequest.metaClass.respondsTo(GrailsWebRequest, "getControllerNamespace").size() > 0) {
            return RequestContextHolder.requestAttributes?.getAttribute(GrailsApplicationAttributes.CONTROLLER_NAMESPACE_ATTRIBUTE, RequestAttributes.SCOPE_REQUEST)
        }
    }

    private String getControllerName() {
        RequestContextHolder.requestAttributes?.controllerName
    }

    private String getActionName() {
        RequestContextHolder.requestAttributes?.actionName
    }

}
