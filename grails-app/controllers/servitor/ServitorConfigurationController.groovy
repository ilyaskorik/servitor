package servitor

import grails.transaction.Transactional
import org.codehaus.groovy.grails.commons.GrailsDomainClass

import static org.springframework.http.HttpStatus.CREATED
import static org.springframework.http.HttpStatus.NOT_FOUND
import static org.springframework.http.HttpStatus.NO_CONTENT
import static org.springframework.http.HttpStatus.OK

@Transactional(readOnly = true)
class ServitorConfigurationController {

//    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    Class domainClass

    def index(Integer max) {

        log.error("index")

        params.max = Math.min(max ?: 10, 100)
        respond domainClass.list(params), model: [instanceCount: domainClass.count()]
    }

    def show() {
        Class clazz = this.getProperty("domainClass")
        GrailsDomainClass dc = grailsApplication.getDomainClass(clazz.name)
        def record = dc.get(params.id)
        respond record
    }

    def create() {
        Class clazz = this.getProperty("domainClass")
        GrailsDomainClass dc = grailsApplication.getDomainClass(clazz.name)
        def newDomainObject = dc.clazz.newInstance(params)
        respond newDomainObject
    }

    @Transactional
    def save() {

        Class clazz = this.getProperty("domainClass")
        GrailsDomainClass dc = grailsApplication.getDomainClass(clazz.name)
        def newDomainObject = dc.clazz.newInstance(params)

        if (newDomainObject == null) {
            notFound()
            return
        }

        if (newDomainObject.hasErrors()) {
            respond newDomainObject.errors, view: 'create'
            return
        }

        newDomainObject.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: "${newDomainObject}.label", default: "${newDomainObject}"), newDomainObject.id])
                redirect userInstance
            }
            '*' { respond newDomainObject, [status: CREATED] }
        }

    }

    def edit() {
        Class clazz = this.getProperty("domainClass")
        GrailsDomainClass dc = grailsApplication.getDomainClass(clazz.name)
        def record = dc.get(params.id)
        respond record
    }

    @Transactional
    def update() {

        Class clazz = this.getProperty("domainClass")
        GrailsDomainClass dc = grailsApplication.getDomainClass(clazz.name)
        def record = dc.get(params.id)

        if (record == null) {
            notFound()
            return
        }

        if (record.hasErrors()) {
            respond record.errors, view: 'edit'
            return
        }

        record.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: "${record}.label", default: "${record}"), record.id])
                redirect record
            }
            '*' { respond record, [status: OK] }
        }
    }

    @Transactional
    def delete() {

        Class clazz = this.getProperty("domainClass")
        GrailsDomainClass dc = grailsApplication.getDomainClass(clazz.name)
        def record = dc.get(params.id)

        if (record == null) {
            notFound()
            return
        }

        record.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: "${record}.label", default: "${record}"), record.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: "${domainClass}.label", default: "${domainClass}"), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }

}
