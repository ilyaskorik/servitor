package servitor

class ServitorBaseFilters {

    def templateUtilsService

    def filters = {
        all(controller: '*', action: '*') {
            before = {

            }
            after = { Map model ->
                String template = templateUtilsService.findTemplate()
                log.error("Template: ${template}")
                if (template) render  model: model, view: template
            }
            afterView = { Exception e ->

            }
        }
    }

}
